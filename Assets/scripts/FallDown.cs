﻿
using UnityEngine;

public class FallDown : MonoBehaviour {
    [SerializeField]
    private float fallSpeed = 3f;

    private void Update()
    {
        if (transform.position.y < -5f)
        {
            Destroy(gameObject);
            if (!Player.lose)
            {
                Player.score++;
            }
        }
        transform.position -= new Vector3(0, fallSpeed * Time.deltaTime,0);
    }
}
