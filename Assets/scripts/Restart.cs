﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Restart : MonoBehaviour {

    private void OnMouseDown()
    {
        Player.score = 0;
        SceneManager.LoadScene("main");
    }
}
