﻿using UnityEngine;

public class Player : MonoBehaviour {

    public GameObject restart;


    public static bool lose = false;
    public static int score = 0;
    private void Awake()
    {
        lose = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bomb")
        {
            lose = true;
            restart.SetActive(true);
        }
    }
}
