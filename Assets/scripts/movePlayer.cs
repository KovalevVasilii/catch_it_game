﻿using UnityEngine;

public class movePlayer : MonoBehaviour {

    public Transform player;
    [SerializeField]
    private float speed = 10f;

    private void OnMouseDrag()
    {
        if (!Player.lose)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (mousePosition.x > 2.5f)
            {
                mousePosition.x = 2.5f;
            }
            else if (mousePosition.x < -2.5f)
            {
                mousePosition.x = -2.5f;
            }
            if (mousePosition.x > player.position.x)
            {
                Debug.Log("Rigth!");
                player.transform.localScale = new Vector2(-1, 1);
            }
            else if (mousePosition.x < player.position.x)
            {
                Debug.Log("Left!");
                player.transform.localScale = new Vector2(1, 1);
            }
            player.position = Vector2.MoveTowards(player.position,
                new Vector2(mousePosition.x, player.position.y), speed * Time.deltaTime);
        }
    }
}
